CREATE SCHEMA `hotornot` ;

CREATE TABLE `hotornot`.`meeting` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `meetingFacilitator` VARCHAR(100) NULL,
	`meetingTitle` VARCHAR(100) NULL,
	`room` INTEGER NULL DEFAULT NULL,
	`score` INTEGER NULL DEFAULT NULL,
	`scheduledTimeStart` TIMESTAMP NULL DEFAULT NULL,
	`scheduledTimeEnd` TIMESTAMP NULL DEFAULT NULL,
	`actualTimeStart` TIMESTAMP NULL DEFAULT NULL,
	`actualTimeEnd` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `hotornot`.`rating` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `rating` INTEGER NOT NULL,
	`room` INTEGER NOT NULL,
	`ratingTimeStamp` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);
