<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Metro UI CSS Framework">
    <meta name="description" content="Simple responsive css framework">
    <meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

    <link href="http://metroui.org.ua/css/metro-bootstrap.css" rel="stylesheet">
    <!--<link href="css/metro-bootstrap-responsive.css" rel="stylesheet">-->
    <link href="http://metroui.org.ua/css/iconFont.css" rel="stylesheet">

    <!-- Load JavaScript Libraries -->
    <script src="http://metroui.org.ua/js/jquery/jquery.min.js"></script>
    <script src="http://metroui.org.ua/js/jquery/jquery.widget.min.js"></script>

    <!-- Metro UI CSS JavaScript plugins -->
    <script src="http://metroui.org.ua/js/load-metro.js"></script>

    <!-- Local JavaScript -->
    <script src="http://metroui.org.ua/js/github.info.js"></script>

    <title>Hot OR Not : HotelsCombined</title>

    <style>
        .container {
            width: 1040px;
        }

        .tileTitle {
        	color: white;
        	margin-left: 3px;
        }

        .divClick {
        {
		  position:absolute;
		  width:100%;
		  height:100%;
		  top:0;
		  left: 0;

		  /* edit: added z-index */
		  z-index: 1;

		  /* edit: fixes overlap error in IE7/8,
		     make sure you have an empty gif */
		  background-image: url('empty.gif');


        }
    </style>
</head>
<body class="metro">
    <div class="container">
        <header class="margin20 nrm nlm">
            <div class="clearfix">

                <a class="place-left" href="#" title="">
                    <h1>Hot or Not</h1>
                </a>
            </div>

        </header>

        <div class="main-content clearfix">
            <div class="tile-area no-padding clearfix">
                <div class="tile-group no-margin no-padding clearfix" style="width: 100%">
                    <div class="tile double quadro-vertical bg-gray ol-transparent" style="float: right; ">
                        <div class="tile-content">

                        </div>
                    </div>

                    <div class="tile quadro double-vertical ol-transparent ${hcTotal}">

                        <div class="tile-content">
                        	<p style="color: white; margin-left: 3px">HotelsCombined</p>
                            <div class="carousel" data-role="carousel" data-height="100%" data-width="100%" data-controls="false">
                               <a href="/chart.html"><span class="divClick"></span></a>
                            </div>
                        </div>

                    </div>

                                            <div class="tile ${tallyRoom1} ol-transparent">
                                            <p style="color: white; margin-left: 3px">Boardroom</p>
										                        <div class="tile-content icon">

										                        </div>
                    </div>

					<div class="tile ol-transparent ${tallyRoom2} margin20">
						  <p style="color: white; margin-left: 3px">Breakout</p>
						<div class="tile-content icon">

						</div>
                    </div>


                    <div class="tile ${tallyRoom3} ol-transparent">
                    	<p style="color: white; margin-left: 3px">Dungeon</p>
                        <div class="tile-content icon">

                        </div>
                    </div>


                    <div class="tile ol-transparent ${tallyRoom4}">
                    	<p style="color: white; margin-left: 3px">The Box</p>
                        <div class="tile-content icon">

                        </div>
                    </div>


                    <div class="tile triple double-vertical ol-transparent bg-white">
                        <div class="tile-content">
                            <div class="panel no-border">
                                <div class="panel-header bg-green fg-white">HOT</div>
                                <div class="panel-content fg-dark nlp nrp">
                                    <img src="images/johnny.png" class="place-left margin10 nlm ntm size2">
                                    <strong>Scrum Master Extraordinaire</strong> Facilitates meetings like a boss. Attending Johnny's meetings is met with as much anticipation as the next episode of Game of Thrones.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tile triple double-vertical ol-transparent bg-white">
                        <div class="tile-content">
                            <div class="panel no-border">
                                <div class="panel-header bg-red fg-white">NOT</div>
                                <div class="panel-content fg-dark nlp nrp">
                                    <img src="images/james.png" class="place-left margin10 nlm ntm size2">
                                    <strong>Yawn Master</strong> Noone has ever made watching grass grow seem more appealling. Bring a good book and a couple fo sleeping tablets.
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End first group -->



    <script src="js/hitua.js"></script>

</body>
</html>