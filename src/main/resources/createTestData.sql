INSERT INTO `hotornot`.`meeting` values (1, 'James Chuck', 'Retrospective', 1, 20, '2014-05-22 08:55:26', '2014-05-22 09:55:26', '2014-05-22 08:55:26', '2014-05-22 09:55:26');
INSERT INTO `hotornot`.`meeting` values (2, 'Johnny Mitrevski', 'Planning', 2, 5, '2014-05-22 09:55:26', '2014-05-22 10:55:26', '2014-05-22 09:55:26', '2014-05-22 10:55:26');
INSERT INTO `hotornot`.`meeting` values (4, 'Ozzy Osborne', 'Interview', 2, 5, '2014-05-22 11:55:26', '2014-05-22 12:55:26', '2014-05-22 11:55:26', '2014-05-22 12:55:26');
INSERT INTO `hotornot`.`meeting` values (5, 'John Madden', 'Review', 3, 90, '2014-05-22 12:55:26', '2014-05-22 13:55:26', '2014-05-22 12:55:26', '2014-05-22 13:55:26');
INSERT INTO `hotornot`.`meeting` values (6, 'Ivan Drew', 'Planning', 3, 90, '2014-05-22 14:55:26', '2014-05-22 15:55:26', '2014-05-22 14:55:26', '2014-05-22 15:55:26');
INSERT INTO `hotornot`.`meeting` values (7, 'Grant Morgan', 'Planning', 2, 65, '2014-05-22 16:55:26', '2014-05-22 17:55:26', '2014-05-22 16:55:26', '2014-05-22 17:55:26');
INSERT INTO `hotornot`.`meeting` values (8, 'Grant Morgan', 'Review', 4, 25, '2014-05-22 17:55:26', '2014-05-22 18:55:26', '2014-05-22 17:55:26', '2014-05-22 18:55:26');
INSERT INTO `hotornot`.`meeting` values (10, 'Grant Morgan', 'Interview', 4, 80, '2014-05-22 18:55:26', '2014-05-22 19:55:26', '2014-05-22 18:55:26', '2014-05-22 19:55:26');
INSERT INTO `hotornot`.`meeting` values (11, 'James Chuck', 'Pre planning', 4, 90, '2014-05-23 07:55:26', '2014-05-23 08:55:26', '2014-05-23 07:55:26', '2014-05-23 08:55:26');
INSERT INTO `hotornot`.`meeting` values (12, 'James Chuck', 'Pre planning', 3, 90, '2014-05-23 08:55:26', '2014-05-23 09:55:26', '2014-05-23 08:55:26', '2014-05-23 09:55:26');
INSERT INTO `hotornot`.`meeting` values (13, 'James Chuck', 'Interview', 3, 80, '2014-05-23 09:55:26', '2014-05-23 10:55:26', '2014-05-23 09:55:26', '2014-05-23 10:55:26');
INSERT INTO `hotornot`.`meeting` values (14, 'Grace Mon', 'Automatcher', 2, 5, '2014-05-23 10:55:26', '2014-05-23 11:55:26', '2014-05-23 10:55:26', '2014-05-23 11:55:26');
INSERT INTO `hotornot`.`meeting` values (15, 'Ivan Drew', 'Market manager session', 2, 20, '2014-05-23 11:55:26', '2014-05-23 12:55:26', '2014-05-23 11:55:26', '2014-05-23 12:55:26');
INSERT INTO `hotornot`.`meeting` values (16, 'Ivan Drew', 'Roadmap session', 4, 50, '2014-05-23 12:55:26', '2014-05-23 13:55:26', '2014-05-23 12:55:26', '2014-05-23 13:55:26');
INSERT INTO `hotornot`.`meeting` values (17, 'Ivan Drew', 'Retrospective', 2, 5, '2014-05-23 13:55:26', '2014-05-23 14:55:26', '2014-05-23 13:55:26', '2014-05-23 14:55:26');
INSERT INTO `hotornot`.`meeting` values (18, 'John Madden', 'Roadmap session', 2, 60, '2014-05-23 08:55:26', '2014-05-23 09:55:26', '2014-05-23 08:55:26', '2014-05-23 09:55:26');
INSERT INTO `hotornot`.`meeting` values (19, 'John Madden', 'Retrospective', 3, 90, '2014-05-23 10:55:26', '2014-05-23 11:55:26', '2014-05-23 10:55:26', '2014-05-23 11:55:26');
INSERT INTO `hotornot`.`meeting` values (20, 'Johnny Mitrevski', 'Standup', 1, null, '2014-05-23 08:55:26', '2014-05-23 09:55:26', '2014-05-23 08:55:26', null);
INSERT INTO `hotornot`.`meeting` values (21, 'Johnny Mitrevski', 'Retrospective', 1, null, '2014-05-23 10:00:26', '2014-05-23 23:55:26', null, null);

INSERT INTO `hotornot`.`rating` values (1, 2, 1, '2014-05-22 09:50:26');
INSERT INTO `hotornot`.`rating` values (2, 0, 1, '2014-05-22 09:51:26');
INSERT INTO `hotornot`.`rating` values (3, 1, 1, '2014-05-22 09:52:26');
INSERT INTO `hotornot`.`rating` values (4, 1, 1, '2014-05-22 09:53:26');
INSERT INTO `hotornot`.`rating` values (5, 0, 1, '2014-05-22 09:54:26');

INSERT INTO `hotornot`.`rating` values (6, 0, 2, '2014-05-22 10:50:26');
INSERT INTO `hotornot`.`rating` values (7, 0, 2, '2014-05-22 10:51:26');
INSERT INTO `hotornot`.`rating` values (8, 1, 2, '2014-05-22 10:52:26');
INSERT INTO `hotornot`.`rating` values (9, 1, 2, '2014-05-22 10:53:26');
INSERT INTO `hotornot`.`rating` values (10, 2, 2, '2014-05-22 10:54:26');

INSERT INTO `hotornot`.`rating` values (11, 2, 1, '2014-05-22 11:55:26');
INSERT INTO `hotornot`.`rating` values (12, 2, 1, '2014-05-22 11:55:26');
INSERT INTO `hotornot`.`rating` values (13, 1, 1, '2014-05-22 11:55:26');
INSERT INTO `hotornot`.`rating` values (14, 1, 1, '2014-05-22 11:55:26');
INSERT INTO `hotornot`.`rating` values (15, 2, 1, '2014-05-22 11:55:26');

INSERT INTO `hotornot`.`rating` values (16, 0, 2, '2014-05-22 12:50:26');
INSERT INTO `hotornot`.`rating` values (17, 0, 2, '2014-05-22 12:51:26');
INSERT INTO `hotornot`.`rating` values (18, 1, 2, '2014-05-22 12:52:26');
INSERT INTO `hotornot`.`rating` values (19, 1, 2, '2014-05-22 12:53:26');
INSERT INTO `hotornot`.`rating` values (20, 0, 2, '2014-05-22 12:54:26');

INSERT INTO `hotornot`.`rating` values (21, 2, 3, '2014-05-22 09:50:26');
INSERT INTO `hotornot`.`rating` values (22, 0, 3, '2014-05-22 09:51:26');
INSERT INTO `hotornot`.`rating` values (23, 1, 3, '2014-05-22 09:52:26');
INSERT INTO `hotornot`.`rating` values (24, 1, 3, '2014-05-22 09:53:26');
INSERT INTO `hotornot`.`rating` values (25, 2, 3, '2014-05-22 09:54:26');

INSERT INTO `hotornot`.`rating` values (26, 2, 4, '2014-05-22 09:50:26');
INSERT INTO `hotornot`.`rating` values (27, 0, 4, '2014-05-22 09:51:26');
INSERT INTO `hotornot`.`rating` values (28, 1, 4, '2014-05-22 09:52:26');
INSERT INTO `hotornot`.`rating` values (29, 1, 4, '2014-05-22 09:53:26');
INSERT INTO `hotornot`.`rating` values (30, 0, 4, '2014-05-22 09:54:26');

INSERT INTO `hotornot`.`rating` values (31, 2, 1, '2014-05-23 08:56:26');
INSERT INTO `hotornot`.`rating` values (32, 2, 1, '2014-05-23 08:57:26');
INSERT INTO `hotornot`.`rating` values (33, 1, 1, '2014-05-23 08:58:26');
INSERT INTO `hotornot`.`rating` values (34, 1, 1, '2014-05-23 08:59:26');
INSERT INTO `hotornot`.`rating` values (35, 0, 1, '2014-05-23 09:00:26');