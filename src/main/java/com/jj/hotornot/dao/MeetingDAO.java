package com.jj.hotornot.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jj.hotornot.entity.Meeting;

/**
 * Abstract and encapsulate the access to the data source for users
 */
@Repository
@Transactional(readOnly = true)
public class MeetingDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = LoggerFactory.getLogger(MeetingDAO.class);

	public Meeting findCurrentMeetingFromTimestamp(int room, Timestamp timestamp) {

		logger.info(this.getClass().getSimpleName() + ".findMeetingFromTimestamp() called timestamp: " + timestamp
				+ ", room " + room);

		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createQuery("from Meeting where room = :room and scheduledTimeStart <= :timestamp AND scheduledTimeEnd >= :timestamp");
		query.setParameter("timestamp", timestamp);
		query.setParameter("room", room);

		Meeting meeting = null;

		try {
			meeting = (Meeting) query.uniqueResult();
		} catch (Exception e) {
			logger.info(this.getClass().getSimpleName() + ".findMeetingFromTimestamp() exception" + e);
		}
		return meeting;
	}

	@SuppressWarnings("unchecked")
	public Meeting findPreviousMeetingFromTimestamp(int room, Timestamp timestamp) {

		logger.info(this.getClass().getSimpleName() + ".findMeetingFromTimestamp() called timestamp: " + timestamp
				+ ", room " + room);

		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createQuery("from Meeting where room = :room AND scheduledTimeEnd < :timestamp Order by scheduledTimeEnd desc");
		query.setParameter("timestamp", timestamp);
		query.setParameter("room", room);

		List<Meeting> meetingList = query.list();

		// Just give me the first one, it should be the previous meeting.
		return meetingList.get(0);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void updateMeeting(Meeting meeting) {

		logger.info(this.getClass().getSimpleName() + ".updateMeeting() called rating: "
				+ meeting.getMeetingFacilitator());

		Session session = sessionFactory.getCurrentSession();

		session.update(meeting);
	}

	@SuppressWarnings("unchecked")
	public List<Meeting> getAllMeetings() {

		logger.info(this.getClass().getSimpleName() + ".getAllMeetings()");

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Meeting");

		List<Meeting> meetingList = query.list();

		return meetingList;
	}
}
