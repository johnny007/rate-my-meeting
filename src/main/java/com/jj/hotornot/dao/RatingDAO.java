package com.jj.hotornot.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jj.hotornot.entity.Rating;

/**
 * Abstract and encapsulate the access to the data source for users
 */
@Repository
@Transactional(readOnly = true)
public class RatingDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = LoggerFactory.getLogger(RatingDAO.class);

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void saveRating(Rating rating) {

		logger.info(this.getClass().getSimpleName() + ".saveRating() called rating: " + rating.getRating());

		Session session = sessionFactory.getCurrentSession();

		session.save(rating);
	}

	@SuppressWarnings("unchecked")
	public List<Rating> getRatingsForTimeRange(Timestamp timestampStart, Timestamp timestampEnd, int room) {

		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createQuery("from Rating where ratingTimeStamp >= :timestampStart AND ratingTimeStamp <= :timestampEnd AND room = :room");
		query.setParameter("timestampStart", timestampStart);
		query.setParameter("timestampEnd", timestampEnd);
		query.setParameter("room", room);

		List<Rating> ratingList = query.list();

		return ratingList;
	}

	@SuppressWarnings("unchecked")
	public List<Rating> getRatingsForRoom(int room) {

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Rating where room = :room");
		query.setParameter("room", room);

		List<Rating> ratingList = query.list();

		return ratingList;
	}

}
