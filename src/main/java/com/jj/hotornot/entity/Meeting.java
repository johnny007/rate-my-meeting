package com.jj.hotornot.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity object encapsulating the meeting table
 */
@Entity
@Table(name = "meeting")
public class Meeting {

	@Id
	@GeneratedValue
	private long id;

	private String meetingFacilitator;
	private String meetingTitle;
	private Integer room;
	private Timestamp scheduledTimeStart;
	private Timestamp scheduledTimeEnd;
	private Timestamp actualTimeStart;
	private Timestamp actualTimeEnd;
	private Integer score;

	public String getMeetingFacilitator() {
		return meetingFacilitator;
	}

	public void setMeetingFacilitator(String meetingFacilitator) {
		this.meetingFacilitator = meetingFacilitator;
	}

	public Timestamp getScheduledTimeStart() {
		return scheduledTimeStart;
	}

	public void setScheduledTimeStart(Timestamp scheduledTimeStart) {
		this.scheduledTimeStart = scheduledTimeStart;
	}

	public Timestamp getScheduledTimeEnd() {
		return scheduledTimeEnd;
	}

	public void setScheduledTimeEnd(Timestamp scheduledTimeEnd) {
		this.scheduledTimeEnd = scheduledTimeEnd;
	}

	public Timestamp getActualTimeStart() {
		return actualTimeStart;
	}

	public void setActualTimeStart(Timestamp actualTimeStart) {
		this.actualTimeStart = actualTimeStart;
	}

	public Timestamp getActualTimeEnd() {
		return actualTimeEnd;
	}

	public void setActualTimeEnd(Timestamp actualTimeEnd) {
		this.actualTimeEnd = actualTimeEnd;
	}

	public String getMeetingTitle() {
		return meetingTitle;
	}

	public void setMeetingTitle(String meetingTitle) {
		this.meetingTitle = meetingTitle;
	}

	public Integer getRoom() {
		return room;
	}

	public void setRoom(Integer room) {
		this.room = room;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
}
