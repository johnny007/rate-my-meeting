package com.jj.hotornot.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity object encapsulating the rating table
 */
@Entity
@Table(name = "rating")
public class Rating {

	@Id
	@GeneratedValue
	private long id;

	private int rating;
	private int room;
	private Timestamp ratingTimeStamp;

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getRoom() {
		return room;
	}

	public void setRoom(int room) {
		this.room = room;
	}

	public Timestamp getRatingTimeStamp() {
		return ratingTimeStamp;
	}

	public void setRatingTimeStamp(Timestamp ratingTimeStamp) {
		this.ratingTimeStamp = ratingTimeStamp;
	}

}
