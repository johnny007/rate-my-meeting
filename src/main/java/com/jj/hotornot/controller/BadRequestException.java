package com.jj.hotornot.controller;

/**
 * The exception to throw when a bad request is made from the user. To be thrown
 * when argument data is invalid.
 */
public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = 5542082906010577923L;

	/**
	 * @param message
	 *            Message describing the error
	 */
	public BadRequestException(String message) {
		super(message);
	}
}
