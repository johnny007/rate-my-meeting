package com.jj.hotornot.controller;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.jj.hotornot.dao.MeetingDAO;
import com.jj.hotornot.dao.RatingDAO;
import com.jj.hotornot.entity.Meeting;
import com.jj.hotornot.entity.Rating;

@Controller
public class ServerEndpointController {

	private static final Logger logger = LoggerFactory.getLogger(ServerEndpointController.class);

	@Autowired
	RatingDAO ratingDAO;

	@Autowired
	MeetingDAO meetingDAO;

	@RequestMapping(value = "/hotornot", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void hotornotEndpoint(@RequestParam("rating") int rating, @RequestParam("room") int room)
			throws UnsupportedEncodingException {

		logger.info(this.getClass().getSimpleName() + " /hotornot called rating: " + rating + ", room: " + room);

		if (room < 0 || room > 4) {
			throw new BadRequestException("room field invalid: " + room);
		}

		if (rating < 0 || rating > 2) {
			throw new BadRequestException("rating field invalid: " + rating);
		}

		Rating myRating = new Rating();

		myRating.setRating(rating);
		myRating.setRatingTimeStamp(getTimestamp());
		myRating.setRoom(room);

		ratingDAO.saveRating(myRating);
	}

	private Timestamp getTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	@RequestMapping(value = "/startMeeting", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void startMeetingEndpoint(@RequestParam("room") int room) throws UnsupportedEncodingException {

		logger.info(this.getClass().getSimpleName() + " /hotornot called room " + room);

		if (room < 0 || room > 4) {
			throw new BadRequestException("room field invalid: " + room);
		}

		Timestamp timestamp = getTimestamp();

		// Start the current meeting
		Meeting meetingCurrent = meetingDAO.findCurrentMeetingFromTimestamp(room, timestamp);

		if (meetingCurrent != null) {
			if (meetingCurrent.getActualTimeStart() == null) {
				startMeeting(timestamp, meetingCurrent);
			} else {
				closeMeeting(timestamp, meetingCurrent);
			}
		}

		// Close the previous meeting
		Meeting meetingPrevious = meetingDAO.findPreviousMeetingFromTimestamp(room, timestamp);

		if (meetingPrevious != null) {
			closeMeeting(timestamp, meetingPrevious);
		}
	}

	private void startMeeting(Timestamp timestamp, Meeting meeting) {

		setActualTimeStart(timestamp, meeting);

		meetingDAO.updateMeeting(meeting);
	};

	private void setActualTimeStart(Timestamp timestamp, Meeting meeting) {

		if (meeting.getActualTimeStart() == null) {
			meeting.setActualTimeStart(timestamp);
		}
	}

	private void closeMeeting(Timestamp timestamp, Meeting meeting) {

		setActualTimeEnd(timestamp, meeting);

		setScore(meeting);

		meetingDAO.updateMeeting(meeting);
	}

	private void setActualTimeEnd(Timestamp timestamp, Meeting meeting) {

		if (meeting.getActualTimeEnd() == null) {
			meeting.setActualTimeEnd(timestamp);
		}
	}

	private void setScore(Meeting meeting) {

		List<Rating> ratings = ratingDAO.getRatingsForTimeRange(meeting.getActualTimeStart(),
				meeting.getActualTimeEnd(), meeting.getRoom());

		int size = ratings.size();

		int tally = 0;

		Iterator<Rating> iterator = ratings.iterator();

		while (iterator.hasNext()) {
			int rating = iterator.next().getRating();

			int ratingAsPercentage = ((rating * 100) / 2);

			tally = tally + ratingAsPercentage;
		}

		int scoreAsPercentage;

		if (size == 0) {
			scoreAsPercentage = 0;
		} else {
			scoreAsPercentage = (tally / size);
		}

		meeting.setScore(scoreAsPercentage);
	}
}
