package com.jj.hotornot.controller;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jj.hotornot.dao.MeetingDAO;
import com.jj.hotornot.dao.RatingDAO;
import com.jj.hotornot.entity.Meeting;
import com.jj.hotornot.entity.Rating;

@Controller
@RequestMapping("/")
public class WebInterfaceController {

	private static final Logger logger = LoggerFactory.getLogger(WebInterfaceController.class);

	@Autowired
	RatingDAO ratingDAO;

	@Autowired
	MeetingDAO meetingDAO;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		logger.info(this.getClass().getSimpleName() + " /client called");

		List<Rating> ratings = ratingDAO.getRatingsForRoom(1);

		List<Meeting> meetings = meetingDAO.getAllMeetings();

		Iterator<Meeting> i = meetings.iterator();

		int size = meetings.size();

		int tally = 0;
		int room1Tally = 0;
		int room1Size = 0;
		int room2Tally = 0;
		int room2Size = 0;
		int room3Tally = 0;
		int room3Size = 0;
		int room4Tally = 0;
		int room4Size = 0;

		try {
			while (i.hasNext()) {
				Meeting meeting = i.next();
				if (meeting != null) {
					tally = tally + meeting.getScore();

					switch (meeting.getRoom()) {
					case 1:
						room1Tally = room1Tally + meeting.getScore();
						room1Size++;
						break;
					case 2:
						room2Tally = room2Tally + meeting.getScore();
						room2Size++;
						break;
					case 3:
						room3Tally = room3Tally + meeting.getScore();
						room3Size++;
						break;
					case 4:
						room4Tally = room4Tally + meeting.getScore();
						room4Size++;
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		int hcTotal = tally / size;

		model.addAttribute("ratings", ratings);
		model.addAttribute("meetings", meetings);
		model.addAttribute("hcTotal", colourScore(hcTotal));
		model.addAttribute("tallyRoom1", colourScore(room1Tally / room1Size));
		model.addAttribute("tallyRoom2", colourScore(room2Tally / room2Size));
		model.addAttribute("tallyRoom3", colourScore(room3Tally / room3Size));
		model.addAttribute("tallyRoom4", colourScore(room4Tally / room4Size));

		return "index";
	}

	private String colourScore(int score) {
		if (score <= 33) {
			return "bg-red";
		} else if (score <= 66) {
			return "bg-amber";
		}

		return "bg-green";
	}
}
