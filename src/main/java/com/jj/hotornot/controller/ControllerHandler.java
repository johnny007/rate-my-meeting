package com.jj.hotornot.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Common handler for exceptions thrown by the application
 */
@ControllerAdvice
public class ControllerHandler extends ResponseEntityExceptionHandler {

	/**
	 * Handle user input validation errors
	 * 
	 * @param bre
	 *            BadRequestException
	 * @return error message
	 */
	@ExceptionHandler({ BadRequestException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public String handleValidationException(BadRequestException bre) {

		String message = "{\"errorMessage\":\"" + bre.getMessage() + "\"}";
		return message;
	}

	/**
	 * Generic handler for RuntimeException errors
	 * 
	 * @param re
	 *            RuntimeException
	 * @return JSON string
	 */
	@ExceptionHandler({ RuntimeException.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public String handleInternaleServerException(RuntimeException re) {

		String message = "{\"errorMessage\":\"hmmm...looks like we can't process your request right now. "
				+ "Please try again.\"}";
		return message;
	}
}
